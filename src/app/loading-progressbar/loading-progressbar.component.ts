import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-progressbar',
  templateUrl: './loading-progressbar.component.html',
  styleUrls: ['./loading-progressbar.component.css']
})
export class LoadingProgressbarComponent implements OnInit {

  @Input() percentage:number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
