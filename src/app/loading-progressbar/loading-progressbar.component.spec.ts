import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingProgressbarComponent } from './loading-progressbar.component';

describe('LoadingProgressbarComponent', () => {
  let component: LoadingProgressbarComponent;
  let fixture: ComponentFixture<LoadingProgressbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoadingProgressbarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoadingProgressbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
