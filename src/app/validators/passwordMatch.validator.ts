import { AbstractControl } from '@angular/forms';

export function ValidatePassword(control: AbstractControl) {
  const password = control.get('password');
  const confirmPassword = control.get('confirmPassword');

  if(password && confirmPassword && password.value !== confirmPassword.value){
    return {missMatch: true};
  }
  return null;
}