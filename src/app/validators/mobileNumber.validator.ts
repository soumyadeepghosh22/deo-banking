import { AbstractControl } from '@angular/forms';

export function ValidateMobileNumber(control: AbstractControl) {
  if(control.value){
    let mobNo = control.value.toString();
    if(mobNo.length < 12 || mobNo.length > 12){
      return {invalid: true};
    }
    if(mobNo.length == 12 && mobNo.substring(0, 2) != "91"){
      return {countryCode: true, input: mobNo.substring(0, 2)};
    }
  }
  return null;
}