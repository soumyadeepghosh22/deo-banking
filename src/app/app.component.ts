import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { block } from './action-blocker';
import { AuthService } from './shared/services/auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit,OnDestroy {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { block(); }
  
  routeSub?:Subscription;
  hasDashboard:boolean = false;
  p:number = 0;
  
  ngOnInit(): void {
    this.routeSub = this.router.events
    .subscribe((event:any) => {
      if(event instanceof NavigationStart){
        this.hasDashboard = (this.authService.isAuthenticated() && event.url.includes('dashboard')) ? true : false;
      }
      if(event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError){
        this.loadingProgressBar();
      }
    });
  }

  loadingProgressBar(){
    this.p = 0;
    let i = setInterval(() => {
      this.p += 10;
    }, 250);
    setTimeout(() => clearInterval(i), 4000);
    this.p = 0;
  }

  ngOnDestroy(): void {
    this.routeSub?.unsubscribe();
  }
}