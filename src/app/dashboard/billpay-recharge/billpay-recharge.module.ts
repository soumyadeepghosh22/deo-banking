import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { BillpayRechargeRoutingModule } from './billpay-recharge-routing.module';

import { BrHomeComponent } from './br-home/br-home.component';

@NgModule({
  declarations: [
    BrHomeComponent
  ],
  imports: [
    CommonModule,
    BillpayRechargeRoutingModule,
    SharedModule
  ]
})
export class BillpayRechargeModule { }
