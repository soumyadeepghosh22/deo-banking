import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrHomeComponent } from './br-home/br-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: BrHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BillpayRechargeRoutingModule { }
