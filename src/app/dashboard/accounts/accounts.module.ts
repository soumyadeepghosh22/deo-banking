import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountsRoutingModule } from './accounts-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { AccountsHomeComponent } from './accounts-home/accounts-home.component';
import { AccountStatementComponent } from './account-statement/account-statement.component';
import { AsFilterComponent } from './account-statement/as-filter/as-filter.component';


@NgModule({
  declarations: [
    AccountsHomeComponent,
    AccountStatementComponent,
    AsFilterComponent
  ],
  imports: [
    CommonModule,
    AccountsRoutingModule,
    SharedModule
  ]
})
export class AccountsModule { }
