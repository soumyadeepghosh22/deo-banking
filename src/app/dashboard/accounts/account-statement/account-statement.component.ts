import { Component, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-account-statement',
  templateUrl: './account-statement.component.html',
  styleUrls: ['./account-statement.component.css'],
  animations: [
    trigger('openClose', [
      state('true', style({display: '*', opacity: 1})),
      state('false', style({display: 'none', opacity: 0})),
      transition('false <=> true', animate('250ms ease-in')),
      transition('true <=> false', animate('250ms ease-out')),
    ])
  ]
})
export class AccountStatementComponent implements OnInit {

  constructor() { }

  isFilter:boolean = false;

  ngOnInit(): void {
  }

  toggleFilter(){
    this.isFilter = !this.isFilter;
  }
}
