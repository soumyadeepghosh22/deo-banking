import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-as-filter',
  templateUrl: './as-filter.component.html',
  styleUrls: ['./as-filter.component.css']
})
export class AsFilterComponent implements OnInit {

  constructor() { }

  statementFilterForm:any;
  periodReadOnly:boolean = true;

  ngOnInit(): void {
    this.statementFilterForm = new FormGroup({
      accountType: new FormControl("", Validators.required),
      accountNo: new FormControl("", Validators.required),
      period: new FormGroup({
        statementType: new FormControl('mini', Validators.required),
        fromDate: new FormControl(null),
        toDate: new FormControl(null)
      }),
      viewType: new FormGroup({
        count: new FormControl("all", Validators.required),
        page: new FormControl(10, Validators.required)
      })
    });
  }

  togglePeriod(){
    this.periodReadOnly = (this.statementFilterForm.value.period.statementType !== 'mini') ? false : true;
  }

  handleFilter(){
  }
}
