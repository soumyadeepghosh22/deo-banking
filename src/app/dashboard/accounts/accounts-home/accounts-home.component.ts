import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-accounts-home',
  templateUrl: './accounts-home.component.html',
  styleUrls: ['./accounts-home.component.css'],
  animations: [
    trigger('openClose', [
      state('true', style({display: '*', opacity: 1})),
      state('false', style({display: 'none', opacity: 0})),
      transition('false <=> true', animate('250ms ease-in')),
      transition('true <=> false', animate('250ms ease-out')),
    ])
  ]
})
export class AccountsHomeComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  isOpen:boolean = false;
  arrowCls:string = 'fa-caret-right';

  ngOnInit(): void {
  }

  checkAccountDetail(){
    this.arrowCls = this.isOpen ? 'fa-caret-right' : 'fa-caret-down';
    this.isOpen = !this.isOpen;
  }

  viewAccountStatement(){
    this.router.navigate(['dashboard','accounts','statement']);
  }

}
