import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MfHomeComponent } from './mf-home.component';

describe('MfHomeComponent', () => {
  let component: MfHomeComponent;
  let fixture: ComponentFixture<MfHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MfHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MfHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
