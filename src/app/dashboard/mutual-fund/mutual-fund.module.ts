import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MutualFundRoutingModule } from './mutual-fund-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { MfHomeComponent } from './mf-home/mf-home.component';


@NgModule({
  declarations: [
    MfHomeComponent
  ],
  imports: [
    CommonModule,
    MutualFundRoutingModule,
    SharedModule
  ]
})
export class MutualFundModule { }
