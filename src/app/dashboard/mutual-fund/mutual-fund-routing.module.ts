import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MfHomeComponent } from './mf-home/mf-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: MfHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MutualFundRoutingModule { }
