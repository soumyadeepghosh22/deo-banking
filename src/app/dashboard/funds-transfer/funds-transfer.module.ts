import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { FundsTransferRoutingModule } from './funds-transfer-routing.module';

import { FtHomeComponent } from './ft-home/ft-home.component';

@NgModule({
  declarations: [
    FtHomeComponent
  ],
  imports: [
    CommonModule,
    FundsTransferRoutingModule,
    SharedModule
  ]
})
export class FundsTransferModule { }
