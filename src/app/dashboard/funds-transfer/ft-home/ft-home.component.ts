import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ft-home',
  templateUrl: './ft-home.component.html',
  styleUrls: ['./ft-home.component.css']
})
export class FtHomeComponent implements OnInit {

  constructor() { }

  listData:any = [
    {
      img: "self-transfer.png",
      title: "Transfer within the bank",
      notes: ['Re 1 Onwards']
    },
    {
      img: "imps-p2a-transfer.png",
      title: "IMPS P2A - Instant Transfer",
      notes: ['Re 1 Onwards', 'IMPS-Account No.']
    },
    {
      img: "neft-transfer.png",
      title: "Transfer to other bank (NEFT)",
      notes: ['Also for Credit Cards Payment', 'Re 1 Onwards']
    },
    {
      img: "rtgs-transfer.png",
      title: "Transfer to other bank (RTGS)",
      notes: ['Rs 2 Lacs Onwards']
    },
    {
      img: "imps-p2p-transfer.png",
      title: "IMPS P2P - Instant Transfer",
      notes: ['IMPS-Mobile No.']
    },
    {
      img: "ecams-transfer.png",
      title: "Transfer to eCAMS Account",
      notes: ['Virtual Account']
    }
  ];

  ngOnInit(): void {
  }

}
