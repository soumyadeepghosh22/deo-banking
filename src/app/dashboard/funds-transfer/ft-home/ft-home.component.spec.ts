import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FtHomeComponent } from './ft-home.component';

describe('FtHomeComponent', () => {
  let component: FtHomeComponent;
  let fixture: ComponentFixture<FtHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FtHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FtHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
