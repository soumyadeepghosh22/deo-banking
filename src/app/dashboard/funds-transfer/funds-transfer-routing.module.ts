import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FtHomeComponent } from './ft-home/ft-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: FtHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FundsTransferRoutingModule { }
