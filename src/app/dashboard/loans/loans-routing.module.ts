import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoansHomeComponent } from './loans-home/loans-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: LoansHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoansRoutingModule { }
