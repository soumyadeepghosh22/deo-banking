import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoansRoutingModule } from './loans-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { LoansHomeComponent } from './loans-home/loans-home.component';

@NgModule({
  declarations: [
    LoansHomeComponent
  ],
  imports: [
    CommonModule,
    LoansRoutingModule,
    SharedModule
  ]
})
export class LoansModule { }
