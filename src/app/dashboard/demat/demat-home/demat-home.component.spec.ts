import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DematHomeComponent } from './demat-home.component';

describe('DematHomeComponent', () => {
  let component: DematHomeComponent;
  let fixture: ComponentFixture<DematHomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DematHomeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DematHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
