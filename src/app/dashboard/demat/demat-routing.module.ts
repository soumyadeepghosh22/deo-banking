import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DematHomeComponent } from './demat-home/demat-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: DematHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DematRoutingModule { }
