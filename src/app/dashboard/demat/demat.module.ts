import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DematRoutingModule } from './demat-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { DematHomeComponent } from './demat-home/demat-home.component';

@NgModule({
  declarations: [
    DematHomeComponent
  ],
  imports: [
    CommonModule,
    DematRoutingModule,
    SharedModule
  ]
})
export class DematModule { }
