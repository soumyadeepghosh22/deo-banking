import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-insurance-home',
  templateUrl: './insurance-home.component.html',
  styleUrls: ['./insurance-home.component.css']
})
export class InsuranceHomeComponent implements OnInit {

  constructor() { }

  listData:any = [
    {
      img: "protection.png",
      title: "Protection",
      url: []
    },
    {
      img: "health.png",
      title: "Health",
      url: []
    },
    {
      img: "children.png",
      title: "Children",
      url: []
    },
    {
      img: "savings-and-investment.png",
      title: "Saving and Investment",
      url: []
    },
    {
      img: "retirement.png",
      title: "Retirement",
      url: []
    }
  ];
  response:boolean = true;

  ngOnInit(): void {
  }

  handleDecision(data:any){
    if(data && data.decision){
      this.response = data.decision === 'yes' ? true : false;
    }
  }
}
