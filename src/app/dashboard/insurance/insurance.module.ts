import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InsuranceRoutingModule } from './insurance-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { InsuranceHomeComponent } from './insurance-home/insurance-home.component';

@NgModule({
  declarations: [
    InsuranceHomeComponent
  ],
  imports: [
    CommonModule,
    InsuranceRoutingModule,
    SharedModule
  ]
})
export class InsuranceModule { }
