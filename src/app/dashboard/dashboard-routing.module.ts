import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'accounts', pathMatch: 'full' },
  { path: '', component: DashboardHomeComponent, children: [
    { path: 'accounts', loadChildren: () => import('./accounts/accounts.module').then(m => m.AccountsModule) },
    { path: 'funds-transfer', loadChildren: () => import('./funds-transfer/funds-transfer.module').then(m => m.FundsTransferModule) },
    { path: 'billpay-recharge', loadChildren: () => import('./billpay-recharge/billpay-recharge.module').then(m => m.BillpayRechargeModule) },
    { path: 'cards', loadChildren: () => import('./cards/cards.module').then(m => m.CardsModule) },
    { path: 'demat', loadChildren: () => import('./demat/demat.module').then(m => m.DematModule) },
    { path: 'mutual-fund', loadChildren: () => import('./mutual-fund/mutual-fund.module').then(m => m.MutualFundModule) },
    { path: 'insurance', loadChildren: () => import('./insurance/insurance.module').then(m => m.InsuranceModule) },
    { path: 'loans', loadChildren: () => import('./loans/loans.module').then(m => m.LoansModule) },
    { path: 'offers', loadChildren: () => import('./offers/offers.module').then(m => m.OffersModule) },
  ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
