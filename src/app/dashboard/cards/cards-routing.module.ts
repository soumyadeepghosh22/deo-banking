import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardsHomeComponent } from './cards-home/cards-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: CardsHomeComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CardsRoutingModule { }
