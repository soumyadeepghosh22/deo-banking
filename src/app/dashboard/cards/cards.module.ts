import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { CardsRoutingModule } from './cards-routing.module';

import { CardsHomeComponent } from './cards-home/cards-home.component';

@NgModule({
  declarations: [
    CardsHomeComponent
  ],
  imports: [
    CommonModule,
    CardsRoutingModule,
    SharedModule
  ]
})
export class CardsModule { }
