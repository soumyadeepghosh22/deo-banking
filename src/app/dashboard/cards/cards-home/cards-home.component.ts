import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards-home',
  templateUrl: './cards-home.component.html',
  styleUrls: ['./cards-home.component.css']
})
export class CardsHomeComponent implements OnInit {

  constructor() { }

  isOpen:boolean = false;
  arrowCls:string = 'fa-caret-right';

  ngOnInit(): void {
  }

  checkAccountDetail(){
    this.arrowCls = this.isOpen ? 'fa-caret-right' : 'fa-caret-down';
    this.isOpen = !this.isOpen;
  }

}
