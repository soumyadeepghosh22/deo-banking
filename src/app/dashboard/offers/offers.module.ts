import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OffersRoutingModule } from './offers-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { OffersHomeComponent } from './offers-home/offers-home.component';

@NgModule({
  declarations: [
    OffersHomeComponent
  ],
  imports: [
    CommonModule,
    OffersRoutingModule,
    SharedModule
  ]
})
export class OffersModule { }
