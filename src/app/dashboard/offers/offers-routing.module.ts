import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OffersHomeComponent } from './offers-home/offers-home.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: OffersHomeComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OffersRoutingModule { }
