import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../shared/shared.module';

import { RetrieveUsernameComponent } from './retrieve-username/retrieve-username.component';
import { RetrievePasswordComponent } from './retrieve-password/retrieve-password.component';
import { RuStep1Component } from './retrieve-username/ru-step1/ru-step1.component';
import { RuStep2Component } from './retrieve-username/ru-step2/ru-step2.component';
import { RuStep3Component } from './retrieve-username/ru-step3/ru-step3.component';
import { RpStep1Component } from './retrieve-password/rp-step1/rp-step1.component';
import { RpStep2Component } from './retrieve-password/rp-step2/rp-step2.component';
import { RpStep3Component } from './retrieve-password/rp-step3/rp-step3.component';
import { RpStep4Component } from './retrieve-password/rp-step4/rp-step4.component';
import { RpStep5Component } from './retrieve-password/rp-step5/rp-step5.component';
import { RpStep6Component } from './retrieve-password/rp-step6/rp-step6.component';


@NgModule({
  declarations: [
    RetrieveUsernameComponent,
    RetrievePasswordComponent,
    RuStep1Component,
    RuStep2Component,
    RuStep3Component,
    RpStep1Component,
    RpStep2Component,
    RpStep3Component,
    RpStep4Component,
    RpStep5Component,
    RpStep6Component
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule
  ]
})
export class PagesModule { }