import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpService } from 'src/app/services/http/http.service';

@Injectable({
  providedIn: 'root'
})
export class RetrieveService {

  constructor(
    private httpService: HttpService
  ) { }

  ruStep:BehaviorSubject<number> = new BehaviorSubject<number>(1);
  rpStep:BehaviorSubject<number> = new BehaviorSubject<number>(1);
  private otp:number = 1234;

  nextRuStep(step:number){
    this.ruStep.next(step);
  }

  nextRpStep(step:number){
    this.rpStep.next(step);
  }

  validateOtp(reqOtp:number){
    return (reqOtp === this.otp) ? true : false;
  }
}
