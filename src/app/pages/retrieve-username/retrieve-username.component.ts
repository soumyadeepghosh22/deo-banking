import { Component, EventEmitter, OnInit } from '@angular/core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-retrieve-username',
  templateUrl: './retrieve-username.component.html',
  styleUrls: ['./retrieve-username.component.css']
})
export class RetrieveUsernameComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  progressArray:string[] = ['active', '', ''];

  ngOnInit(): void {
    this.router.events
    .subscribe((event:any) => {
      if(event instanceof NavigationStart){
        let url = event.url;
        if(url.includes('step2')){
          this.progressArray = ['finish', 'active', ''];
        }else if(url.includes('step3')){
          this.progressArray = ['finish', 'finish', 'active'];
        }else{
          this.progressArray = ['active', '', ''];
        }
      }
    })
  }

  handleMultiStepSubmit(){
    // this.formStep += 1;
    // if(this.formStep === 2){
    //   this.progressArray = ['finish','active',''];
    // }else if(this.formStep === 3){
    //   this.progressArray = ['finish','finish','active'];
    // }
  }
}
