import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-ru-step2',
  templateUrl: './ru-step2.component.html',
  styleUrls: ['./ru-step2.component.css']
})
export class RuStep2Component implements OnInit,OnDestroy {

  constructor(
    private router: Router,
    private retrieveService: RetrieveService,
    private notificationService: NotificationService
  ) { }

  ruFormStep2:any;
  sub?:Subscription;
  otpBtnDisabled:boolean = true;
  timer:number = environment.OTP_TIMER/1000;

  ngOnInit(): void {
    this.setTimer();
    this.sub = this.retrieveService.ruStep
    .subscribe((res:any) => {
      if(res !== 2){
        this.router.navigate(['login']);
      }
    });
    this.ruFormStep2 = new FormGroup({
      otp: new FormControl(null, [Validators.required, Validators.min(1000), Validators.max(9999)])
    })
  }

  setTimer(){
    let interval = setInterval(() => {
      this.timer -= 1;
    }, 1000);
    setTimeout(() => {
      this.otpBtnDisabled = false;
      clearInterval(interval);
    }, environment.OTP_TIMER);
    this.timer = environment.OTP_TIMER/1000;
  }

  resendOtp(){
    this.otpBtnDisabled = true;
    this.setTimer();
  }

  handleSubmit(){
    if(this.retrieveService.validateOtp(this.ruFormStep2.value.otp)){
      this.retrieveService.nextRuStep(3);
      this.notificationService.showSuccess('Step 2 has been completed!');
      this.router.navigate(['netbanking','forgot-customer-id','step3']);
    }else{
      this.notificationService.showError('Wrong OTP!');
    }
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
