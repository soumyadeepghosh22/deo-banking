import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RuStep2Component } from './ru-step2.component';

describe('RuStep2Component', () => {
  let component: RuStep2Component;
  let fixture: ComponentFixture<RuStep2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RuStep2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RuStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
