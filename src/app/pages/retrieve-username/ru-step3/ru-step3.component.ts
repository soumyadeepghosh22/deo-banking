import { Component, OnDestroy, OnInit } from '@angular/core';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NotificationService } from 'src/app/services/notification/notification.service';

@Component({
  selector: 'app-ru-step3',
  templateUrl: './ru-step3.component.html',
  styleUrls: ['./ru-step3.component.css']
})
export class RuStep3Component implements OnInit,OnDestroy {

  constructor(
    private router: Router,
    private retrieveService: RetrieveService,
    private notificationService: NotificationService
  ) { }

  sub?:Subscription;

  ngOnInit(): void {
    this.retrieveService.ruStep
    .subscribe((res:any) => {
      if(res !== 3){
        this.router.navigate(['login']);
      }
    });
  }

  copyCustomerId(copyText:string){
    navigator.clipboard.writeText(copyText);
    this.notificationService.showInfo('Customer ID copied');
  }

  goToLogin(){
    this.retrieveService.nextRuStep(0);
    this.notificationService.showSuccess('Step 3 has been completed!');
    this.router.navigate(['login']);
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
