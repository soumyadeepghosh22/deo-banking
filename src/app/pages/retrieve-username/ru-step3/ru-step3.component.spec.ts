import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RuStep3Component } from './ru-step3.component';

describe('RuStep3Component', () => {
  let component: RuStep3Component;
  let fixture: ComponentFixture<RuStep3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RuStep3Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RuStep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
