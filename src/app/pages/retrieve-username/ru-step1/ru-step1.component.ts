import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { ValidateMobileNumber } from 'src/app/validators/mobileNumber.validator';

@Component({
  selector: 'app-ru-step1',
  templateUrl: './ru-step1.component.html',
  styleUrls: ['./ru-step1.component.css']
})
export class RuStep1Component implements OnInit {

  constructor(
    private router: Router,
    private retrieveService: RetrieveService,
    private notificationService: NotificationService
  ) { }

  ruFormStep1:any;
  currentDate:string = new Date().getFullYear()+'-'+((new Date().getMonth() > 8) ? (new Date().getMonth()+1) : ('0'+(new Date().getMonth()+1)))+'-'+((new Date().getDate() > 9) ? new Date().getDate() : ('0'+new Date().getDate()));
  isValidMob:boolean = false;
  captchaValidation:boolean = false;
  isSubmitted:boolean = false;

  ngOnInit(): void {
    this.ruFormStep1 = new FormGroup({
      mobileNumber: new FormControl(null, [Validators.required, ValidateMobileNumber]),
      dob: new FormControl(null, Validators.required),
      panNumber: new FormControl(null, [Validators.required, Validators.pattern('[A-Z]{5}[0-9]{4}[A-Z]{1}')])
    })
  }

  handleCaptchaError(res:any){
    this.captchaValidation = res;
  }

  handleSubmit(){
    this.isSubmitted = true;
    if(this.captchaValidation){
      this.retrieveService.nextRuStep(2);
      this.notificationService.showSuccess('Step 1 has been completed!');
      this.router.navigate(['netbanking','forgot-customer-id','step2']);
    }
  }
}
