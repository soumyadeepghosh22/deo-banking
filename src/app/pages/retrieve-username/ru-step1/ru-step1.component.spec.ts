import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RuStep1Component } from './ru-step1.component';

describe('RuStep1Component', () => {
  let component: RuStep1Component;
  let fixture: ComponentFixture<RuStep1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RuStep1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RuStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
