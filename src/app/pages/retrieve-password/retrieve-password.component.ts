import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-retrieve-password',
  templateUrl: './retrieve-password.component.html',
  styleUrls: ['./retrieve-password.component.css']
})
export class RetrievePasswordComponent implements OnInit {

  constructor() { }

  formStep:number = 1;

  ngOnInit(): void {
  }

  changeFormStep(step:number){
    this.formStep = step;
  }
}
