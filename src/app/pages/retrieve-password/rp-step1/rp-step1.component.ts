import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rp-step1',
  templateUrl: './rp-step1.component.html',
  styleUrls: ['./rp-step1.component.css']
})
export class RpStep1Component implements OnInit {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  rpFormStep1:any;
  sub?:Subscription;

  ngOnInit(): void {
    this.rpFormStep1 = new FormGroup({
      customerId: new FormControl(null, Validators.required),
    })
  }

  handleSubmit(){
    this.retrieveService.nextRpStep(2);
    this.notificationService.showSuccess('Step 1 has been completed!');
    this.router.navigate(['netbanking','forgot-password','step2']);
  }
}
