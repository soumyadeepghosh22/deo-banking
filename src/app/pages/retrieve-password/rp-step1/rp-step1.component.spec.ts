import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep1Component } from './rp-step1.component';

describe('RpStep1Component', () => {
  let component: RpStep1Component;
  let fixture: ComponentFixture<RpStep1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep1Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
