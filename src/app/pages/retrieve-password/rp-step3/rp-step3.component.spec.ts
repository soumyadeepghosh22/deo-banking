import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep3Component } from './rp-step3.component';

describe('RpStep3Component', () => {
  let component: RpStep3Component;
  let fixture: ComponentFixture<RpStep3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep3Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
