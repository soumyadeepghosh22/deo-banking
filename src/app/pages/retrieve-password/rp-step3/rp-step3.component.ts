import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { Router } from '@angular/router';
import { ValidateMobileNumber } from 'src/app/validators/mobileNumber.validator';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rp-step3',
  templateUrl: './rp-step3.component.html',
  styleUrls: ['./rp-step3.component.css']
})
export class RpStep3Component implements OnInit,OnDestroy {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  rpFormStep3:any;
  sub?:Subscription;

  ngOnInit(): void {
    this.sub = this.retrieveService.rpStep
    .subscribe((res:any) => {
      if(res !== 3){
        this.router.navigate(['login']);
      }
    });
    this.rpFormStep3 = new FormGroup({
      mobileNumber: new FormControl(null, [Validators.required, ValidateMobileNumber])
    })
  }

  handleSubmit(){
    this.retrieveService.nextRpStep(4);
    this.notificationService.showSuccess('Step 3 has been completed!');
    this.router.navigate(['netbanking','forgot-password','step4']);
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
