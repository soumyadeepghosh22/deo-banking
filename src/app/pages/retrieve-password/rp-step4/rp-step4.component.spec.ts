import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep4Component } from './rp-step4.component';

describe('RpStep4Component', () => {
  let component: RpStep4Component;
  let fixture: ComponentFixture<RpStep4Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep4Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
