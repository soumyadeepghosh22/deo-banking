import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-rp-step4',
  templateUrl: './rp-step4.component.html',
  styleUrls: ['./rp-step4.component.css']
})
export class RpStep4Component implements OnInit,OnDestroy {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  rpFormStep4:any;
  otpBtnDisabled:boolean = true;
  timer:number = environment.OTP_TIMER/1000;
  sub?:Subscription;

  ngOnInit(): void {
    this.sub = this.retrieveService.rpStep
    .subscribe((res:any) => {
      if(res !== 4){
        this.router.navigate(['login']);
      }
    });
    this.setTimer();
    this.rpFormStep4 = new FormGroup({
      otp: new FormControl(null, [Validators.required, Validators.min(1000), Validators.max(9999)])
    })
  }

  setTimer(){
    let interval = setInterval(() => {
      this.timer -= 1;
    }, 1000);
    setTimeout(() => {
      this.otpBtnDisabled = false;
      clearInterval(interval);
    }, environment.OTP_TIMER);
    this.timer = environment.OTP_TIMER/1000;
  }

  resendOtp(){
    this.otpBtnDisabled = true;
    this.setTimer();
  }

  handleSubmit(){
    if(this.retrieveService.validateOtp(this.rpFormStep4.value.otp)){
      this.retrieveService.nextRpStep(5);
      this.notificationService.showSuccess('Step 4 has been completed!');
      this.router.navigate(['netbanking','forgot-password','step5']);
    }else{
      this.notificationService.showError('Wrong OTP!');
    }
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
