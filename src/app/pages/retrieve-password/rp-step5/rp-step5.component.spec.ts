import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep5Component } from './rp-step5.component';

describe('RpStep5Component', () => {
  let component: RpStep5Component;
  let fixture: ComponentFixture<RpStep5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep5Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
