import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ValidatePassword } from 'src/app/validators/passwordMatch.validator';

@Component({
  selector: 'app-rp-step5',
  templateUrl: './rp-step5.component.html',
  styleUrls: ['./rp-step5.component.css']
})
export class RpStep5Component implements OnInit,OnDestroy {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  days:string[] = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  years:number[] = [];
  rpFormStep5:any;
  sub?:Subscription;

  ngOnInit(): void {
    for(let year=new Date().getFullYear(); year<=(new Date().getFullYear()+10); year++){
      this.years.push(year);
    }
    this.sub = this.retrieveService.rpStep
    .subscribe((res:any) => {
      if(res !== 5){
        this.router.navigate(['login']);
      }
    });
    this.rpFormStep5 = new FormGroup({
      cardNumber: new FormControl("1234567890123456", Validators.required),
      atmPin: new FormControl(null, [Validators.required, Validators.pattern("[0-9]{4}")]),
      expiryDate: new FormGroup({
        month: new FormControl("", Validators.required),
        year: new FormControl("", Validators.required)
      }),
      ipin: new FormGroup({
        password: new FormControl(null, [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,15}")]),
        confirmPassword: new FormControl(null, [Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,15}")])
      }, {validators: ValidatePassword}),
      tNc: new FormControl(null, Validators.requiredTrue)
    })
  }

  handleSubmit(){
    this.retrieveService.nextRpStep(6);
    this.notificationService.showSuccess('Step 5 has been completed!');
    this.router.navigate(['netbanking','forgot-password','step6']);
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
