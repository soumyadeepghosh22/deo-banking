import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rp-step6',
  templateUrl: './rp-step6.component.html',
  styleUrls: ['./rp-step6.component.css']
})
export class RpStep6Component implements OnInit,OnDestroy {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  sub?:Subscription;

  ngOnInit(): void {
    this.sub = this.retrieveService.rpStep
    .subscribe((res:any) => {
      if(res !== 6){
        this.router.navigate(['login']);
      }
    });
  }

  goToLogin(){
    this.retrieveService.nextRpStep(0);
    this.notificationService.showSuccess('Step 6 has been completed!');
    this.router.navigate(['login']);
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
