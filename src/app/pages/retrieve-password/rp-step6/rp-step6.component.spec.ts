import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep6Component } from './rp-step6.component';

describe('RpStep6Component', () => {
  let component: RpStep6Component;
  let fixture: ComponentFixture<RpStep6Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep6Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
