import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RetrieveService } from '../../services/retrieve/retrieve.service';
import { NotificationService } from 'src/app/services/notification/notification.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-rp-step2',
  templateUrl: './rp-step2.component.html',
  styleUrls: ['./rp-step2.component.css']
})
export class RpStep2Component implements OnInit,OnDestroy {

  constructor(
    private retrieveService: RetrieveService,
    private notificationService: NotificationService,
    private router: Router
  ) { }

  rpFormStep2:any;
  captchaValidation:boolean = false;
  isSubmitted:boolean = false;
  sub?:Subscription;

  ngOnInit(): void {
    this.sub = this.retrieveService.rpStep
    .subscribe((res:any) => {
      if(res !== 2){
        this.router.navigate(['login']);
      }
    });
    this.rpFormStep2 = new FormGroup({
      authOption: new FormControl("auth1", Validators.required)
    })
  }

  handleCaptchaError(res:any){
    this.captchaValidation = res;
  }

  handleSubmit(){
    this.isSubmitted = true;
    if(this.rpFormStep2.value.authOption === "auth1"){
      if(this.captchaValidation){
        this.retrieveService.nextRpStep(3);
        this.notificationService.showSuccess('Step 2 has been completed!');
        this.router.navigate(['netbanking','forgot-password','step3']);
      }
    }else{
      this.notificationService.showError('Sorry! OTP Authentication with Mobile No. & Email ID is not active yet.');
    }
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
