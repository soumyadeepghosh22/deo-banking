import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpStep2Component } from './rp-step2.component';

describe('RpStep2Component', () => {
  let component: RpStep2Component;
  let fixture: ComponentFixture<RpStep2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RpStep2Component ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RpStep2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
