import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetrieveUsernameComponent } from './retrieve-username/retrieve-username.component';
import { RetrievePasswordComponent } from './retrieve-password/retrieve-password.component';
import { RuStep1Component } from './retrieve-username/ru-step1/ru-step1.component';
import { RuStep2Component } from './retrieve-username/ru-step2/ru-step2.component';
import { RuStep3Component } from './retrieve-username/ru-step3/ru-step3.component';
import { RpStep1Component } from './retrieve-password/rp-step1/rp-step1.component';
import { RpStep2Component } from './retrieve-password/rp-step2/rp-step2.component';
import { RpStep3Component } from './retrieve-password/rp-step3/rp-step3.component';
import { RpStep4Component } from './retrieve-password/rp-step4/rp-step4.component';
import { RpStep5Component } from './retrieve-password/rp-step5/rp-step5.component';
import { RpStep6Component } from './retrieve-password/rp-step6/rp-step6.component';

const routes: Routes = [
  { path: 'forgot-customer-id', component: RetrieveUsernameComponent, children: [
    { path: 'step1', component: RuStep1Component },
    { path: 'step2', component: RuStep2Component },
    { path: 'step3', component: RuStep3Component }
  ] },
  { path: 'forgot-password', component: RetrievePasswordComponent, children: [
    { path: 'step1', component: RpStep1Component },
    { path: 'step2', component: RpStep2Component },
    { path: 'step3', component: RpStep3Component },
    { path: 'step4', component: RpStep4Component },
    { path: 'step5', component: RpStep5Component },
    { path: 'step6', component: RpStep6Component }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
