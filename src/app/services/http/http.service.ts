import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  apiUrl:string = environment.API_URL;

  getAll(url:string){
    return this.http.get(`${this.apiUrl}/${url}`);
  }

  getOne(url:string, uid:any){
    return this.http.get(`${this.apiUrl}/${url}/${uid}`);
  }

  post(url:string, data:any){
    return this.http.post(`${this.apiUrl}/${url}`, data);
  }

  put(url:string, uid:any, data:any){
    return this.http.put(`${this.apiUrl}/${url}/${uid}`, data);
  }

  delete(url:string, uid:any){
    return this.http.delete(`${this.apiUrl}/${url}/${uid}`);
  }
}
