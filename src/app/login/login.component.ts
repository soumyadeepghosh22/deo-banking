import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from './Login';
import { Subscription } from 'rxjs';
import { AuthService } from '../shared/services/auth/auth.service';
import { NotificationService } from '../services/notification/notification.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthData } from '../shared/services/auth/AuthData';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit,OnDestroy {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    private notificationService: NotificationService
  ) { }

  display1:string = '';
  display2:string = 'd-none';
  customerIdReadOnly:boolean = false;
  model:Login = {customerId: '', password: ''};
  checkCustomerIdSub?:Subscription;
  loginSub?:Subscription;

  ngOnInit(): void {
  }

  toForgotCustomerId(){
    this.router.navigate(['netbanking','forgot-customer-id','step1']);
  }

  toForgotPassword(){
    this.router.navigate(['netbanking','forgot-password', 'step1']);
  }

  modifyCustomerId(){
    this.display1 = '';
    this.display2 = 'd-none';
    this.customerIdReadOnly = false;
  }

  continue(){
    this.checkCustomerIdSub = this.authService.checkCustomerIdExist(this.model.customerId)
    .subscribe({
      next: () => {
        this.display1 = 'd-none';
        this.display2 = '';
        this.customerIdReadOnly = true;
      },
      error: (err:HttpErrorResponse) => {
        this.notificationService.showError(err.error.message);
      }
    })
  }

  handleLogin(){
    this.loginSub = this.authService.login(this.model)
    .subscribe({
      next: (res:any) => {
        let authData = { authCustomer: this.model.customerId, authToken: res.authToken };
        this.authService.setAuthInfo(authData);
        this.notificationService.showSuccess(res.message);
        let rawUrl = this.route.snapshot.queryParams['url'];
        let navigateUrl = rawUrl ? rawUrl : '/dashboard/accounts';
        this.router.navigateByUrl(navigateUrl);
      },
      error: (err:HttpErrorResponse) => {
        this.notificationService.showError(err.error.message);
      }
    });
  }

  ngOnDestroy(): void {
    this.loginSub?.unsubscribe();
  }
}
