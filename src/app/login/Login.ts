export interface Login {
    customerId: string;
    password: string;
}