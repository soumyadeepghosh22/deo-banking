function rightClickBlock(){
    document.addEventListener("contextmenu", function(e){
        alert('Right click is not allowed');
        e.preventDefault();
    }, false);
}

function textSelectionBlock(){
    document.addEventListener("selectstart", function(e){
        e.preventDefault();
    }, false);
}

export function block(){
    rightClickBlock();
    textSelectionBlock();
}