import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() img:any;
  @Input() htmlTemplate:TemplateRef<any> | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
