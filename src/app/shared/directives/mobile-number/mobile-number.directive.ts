import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appMobileNumber]'
})
export class MobileNumberDirective {

  constructor(
    private el: ElementRef
  ) { }

  @HostListener('keypress', ['$event']) onKeyPress(){
    setTimeout(() => {
      this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^0-9]/g, '');
    }, 100)
  }

  @HostListener('paste', ['$event']) onPaste(){
    setTimeout(() => {
      this.el.nativeElement.value = null;
    }, 100)
  }
}