import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appCustomerId]'
})
export class CustomerIdDirective {

  constructor(
    private el: ElementRef
  ) { }

  @HostListener('keypress', ['$event']) onKeyPress(event:KeyboardEvent){
    this,this.preventType(event);
  }

  @HostListener('paste', ['$event']) onPaste(event:KeyboardEvent){
    this,this.preventType(event);
  }

  private preventType(event:any){
    setTimeout(() => {
      let customerId = this.el.nativeElement.value;
      if(customerId && customerId.length <= 8){
        event.preventDefault();
        this.el.nativeElement.value = customerId;
      }else{
        this.el.nativeElement.value = customerId.slice(0, 8);
      }
    }, 100)
  }
}
