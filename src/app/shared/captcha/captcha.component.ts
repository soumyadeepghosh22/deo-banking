import { Component, Input, Output, OnDestroy, OnInit, EventEmitter } from '@angular/core';
import { CaptchaService } from '../services/captcha/captcha.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnInit,OnDestroy {

  @Input() isSubmitted = false;
  @Output() captchaValidation:EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private captchaService: CaptchaService
  ) { }

  captchaSub?:Subscription;
  captchaCode:string = '';
  captchaInput:string = '';
  hasError:boolean = false;

  ngOnInit(): void {
    this.getCaptcha('load');
  }

  getCaptcha(event:any){
    this.captchaValidation.emit(false);
    this.hasError = true;
    this.captchaService.getCaptcha;
    this.captchaSub = this.captchaService.captchaData
    .subscribe((res:any) => {
      this.captchaCode = res;
    });
    if(event === 'click') document.getElementById('captchaInput')?.focus();
  }

  getVoiceCaptcha(){
    let speakData = new SpeechSynthesisUtterance();
    speakData.volume = 1;
    speakData.rate = 0.5;
    speakData.pitch = 2;
    speakData.text = this.captchaCode;
    speakData.lang = 'en';
    speechSynthesis.speak(speakData);
  }

  validate(){
    let validateCaptcha = this.captchaService.validateCaptcha(this.captchaInput);
    this.captchaValidation.emit(validateCaptcha);
    this.hasError = !validateCaptcha;
  }

  ngOnDestroy(): void {
    this.captchaSub?.unsubscribe();
  }
}
