import { Component, OnInit, Input, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit {

  @Input() htmlTemplate:TemplateRef<any> | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
