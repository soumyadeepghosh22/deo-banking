import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth/auth.service';
import { Welcome } from './Welcome';
import { environment } from 'src/environments/environment';
import { NotificationService } from 'src/app/services/notification/notification.service';

@Component({
  selector: 'app-topnav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.component.css']
})
export class TopnavComponent implements OnInit,OnDestroy {

  @Input() navmenus:any;

  constructor(
    private router: Router,
    private authService: AuthService,
    private notificationService: NotificationService
  ) { }

  customerId:string = "";
  welcomeData:Welcome = {firstName: '', lastName: '', lastLoginTime: null};
  logoutPopup:boolean = false;
  userInfoSub?:Subscription;
  sessionSub?:Subscription;

  ngOnInit(): void {
    this.customerId = this.authService.getAuthInfo().authCustomer;
    this.userInfoSub = this.authService.getUserInfo(this.customerId)
    .subscribe((res:any) => {
      this.welcomeData = res.data;
    });
    setTimeout(() => this.handleLogout('load'), environment.SESSION_TIME);
  }

  toggleLogoutPopup(){
    this.logoutPopup = !this.logoutPopup;
  }

  saveActivityTimestamp(){
    this.sessionSub = this.authService.logout(this.customerId).subscribe();
  }

  handleLogout(event:string){
    if(event === 'click'){
      this.toggleLogoutPopup();
    }
    this.saveActivityTimestamp();
    this.authService.removeAuthInfo();
    if(event === 'load'){
      window.location.reload();
      this.notificationService.showError("Session Timedout");
    }else{
      this.router.navigate(['/login']);
      setTimeout(() => window.location.reload(), 1000);
      this.notificationService.showSuccess("Logged out successfully");
    }
  }

  ngOnDestroy(): void {
    this.userInfoSub?.unsubscribe();
    this.sessionSub?.unsubscribe();
  }
}
