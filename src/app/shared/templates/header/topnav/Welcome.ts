export interface Welcome {
    firstName: string;
    lastName: string;
    lastLoginTime: Date|null;
}