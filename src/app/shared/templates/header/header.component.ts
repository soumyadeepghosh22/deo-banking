import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { Menus } from './menus/Menus';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit,OnDestroy {

  @Input() hasDashboard?:boolean;

  constructor(
    private router: Router
  ) { }

  navmenus:any = Menus.getTopNav();
  sidemenusAll:any = Menus.getSideNav();
  sidemenus:any = [];
  navSub?:Subscription;

  ngOnInit(): void {
    this.getSideNav(this.router.url);
    this.navSub = this.router.events
    .subscribe((event:any) => {
      if(event instanceof NavigationStart){
        this.getSideNav(event.url);
      }
    });
  }

  getSideNav(url:any){
    let urlArray = url.split('/');
    if(urlArray && urlArray[2] && this.sidemenusAll[urlArray[2]]){
      this.sidemenus = this.sidemenusAll[urlArray[2]];
    }else{
      this.sidemenus = this.sidemenusAll['accounts'];
    }
  }

  ngOnDestroy(): void {
    this.navSub?.unsubscribe();
  }
}
