import { Component, OnInit, Input, ElementRef } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  @Input() sidemenus:any;

  constructor(
    private el: ElementRef
  ) { }

  Object:any = Object;

  ngOnInit(): void {
  }

  toggleSubMenu(id:any){
    let icons = this.el.nativeElement.querySelectorAll('.m-icon');
    let icon = this.el.nativeElement.querySelector('#icon'+id);
    let smenus = this.el.nativeElement.querySelectorAll('.submenu');
    let smenu = this.el.nativeElement.querySelector('#sm'+id);
    icons.forEach((i:any) => {
      $(i).not(icon).removeClass('fa-caret-down');
      $(i).not(icon).addClass('fa-caret-right');
    });
    smenus.forEach((e:any) => {
      $(e).not(smenu).slideUp();
    });
    $(icon).toggleClass("fa-caret-down fa-caret-right");
    $(smenu).slideToggle();
  }
}
