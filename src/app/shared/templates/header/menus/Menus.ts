import { topnav } from "./properties/topnav.properties";
import { sidenav } from "./properties/sidenav.properties";

export class Menus {
    public static getTopNav(){
        return topnav;
    }

    public static getSideNav(){
        return sidenav;
    }
}