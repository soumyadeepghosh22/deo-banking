export const sidenav = {
    "accounts": {
        "Accounts": [
            {
                title: "Account Summary",
                url: ["dashboard", "accounts", "home"]
            },
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "Funds Transfer to Own Accounts", url: [] },
                    { subtitle: "Open Fixed Deposit < Rs 5 Cr", url: [] },
                    { subtitle: "Open Recurring Deposit", url: [] },
                    { subtitle: "Open New FCNR Deposit", url: [] },
                    { subtitle: "Fixed Deposit Sweep-in", url: [] },
                    { subtitle: "Overdraft against FD", url: [] },
                    { subtitle: "Liquidate Fixed Deposit", url: [] },
                    { subtitle: "Liquidate RD/My Passion Fund", url: [] },
                    { subtitle: "Liquidate FCNR Deposit", url: [] },
                    { subtitle: "CASA Sweepin", url: [] },
                    { subtitle: "Open My Passion Fund", url: [] },
                    { subtitle: "Top Up add to My Passion Fund", url: [] },
                    { subtitle: "Remove FD Overdraft", url: [] },
                    { subtitle: "Foreign Currency Inward Remittance", url: [] },
                    { subtitle: "Invest in RBI Bonds", url: [] },
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    { subtitle: "View Account Balance", url: [] },
                    { subtitle: "A/c Statement - Current & Previous Month", url: [] },
                    { subtitle: "A/c Statement - Upto 5 Years", url: [] },
                    { subtitle: "A/c Statement - Upto 10 Years", url: [] },
                    { subtitle: "Fixed Deposit Summary", url: [] },
                    { subtitle: "Recurring Deposit Summary", url: [] },
                    { subtitle: "View Cheque Status", url: [] },
                    { subtitle: "TDS Inquiry", url: [] },
                    { subtitle: "Hold Enquiry", url: [] },
                    { subtitle: "Mailbox", url: [] },
                    { subtitle: "My Passion Fund Summary", url: [] },
                    { subtitle: "View Tax Credit Statements", url: [] },
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "View / Add GST No.", url: [] },
                    { subtitle: "Request CIBIL TU Score", url: [] },
                    { subtitle: "Confirm KYC Details", url: [] },
                    { subtitle: "Update Extended KYC", url: [] },
                    { subtitle: "Stop Payment of Cheque", url: [] },
                    { subtitle: "Cheque Book", url: [] },
                    { subtitle: "Demand Draft", url: [] },
                    { subtitle: "Add / Update PAN Number", url: [] },
                    { subtitle: "Email Statement Registration", url: [] },
                    { subtitle: "Form 15 G/H", url: [] },
                    { subtitle: "Interest Certificate Download", url: [] },
                    { subtitle: "Download Locker Nomination Form", url: [] },
                    { subtitle: "Generate MMID", url: [] },
                    { subtitle: "RetrIeve MMID", url: [] },
                    { subtitle: "Cancel MMID", url: [] },
                    { subtitle: "Regenerate Direct Tax Challans", url: [] },
                    { subtitle: "Regenerate InDirect Tax Challans", url: [] },
                    { subtitle: "Modify Secure Access Profile", url: [] },
                    { subtitle: "View / Update Nomination Details", url: [] },
                    { subtitle: "Change FD Instruction", url: [] },
                    { subtitle: "Download Balance Certificate", url: [] },
                    { subtitle: "Apply For DTAA", url: [] },
                    { subtitle: "Change RD Instruction", url: [] },
                    { subtitle: "Download Mandate Form", url: [] },
                    { subtitle: "Download Deposit Slip", url: [] },
                    { subtitle: "Smart Slips", url: [] },
                    { subtitle: "Income Tax E-Filling", url: [] },
                    { subtitle: "Positive Pay", url: [] },
                    { subtitle: "IPO / Right Issue New", url: [] },
                ]
            },
        ]
    },
    "funds-transfer": {
        "Funds Transfer": [
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "Third Party Funds Transfer", url: [] },
                    { subtitle: "VISA CardPay", url: [] },
                    { subtitle: "Repatriation of Funds", url: [] },
                    { subtitle: "Resend OTP Cardless Cash withdrawal", url: [] },
                    
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    { subtitle: "View/Delete List of Beneficiaries", url: [] },
                    { subtitle: "OTP Pending Transactions", url: [] },
                    { subtitle: "View RTGS Funds Transfer", url: [] },
                    { subtitle: "View IMPS Funds Transfer", url: [] },
                    { subtitle: "Repatriation of Funds (View)", url: [] },
                    { subtitle: "View Order Status Cardless Withdrawal", url: [] },
                    
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "Change Image and Message", url: [] },
                    { subtitle: "Change Question Answers", url: [] },
                    { subtitle: "Add a Beneficiary", url: [] },
                    { subtitle: "View/Delete Standing Instructions", url: [] },
                    { subtitle: "Modify TPT Limit", url: [] },
                    { subtitle: "Repatriation of Funds (Add Beneficiary)", url: [] },
                    { subtitle: "Digital Certificate Registration", url: [] },
                    
                ]
            }
        ]
    },
    "billpay-recharge": {
        "BillPay & Recharge": []
    },
    "cards": {
        "Credit Cards": [
            {
                title: "Account Summary",
                url: ["dashboard", "cards", "home"]
            },
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "Credit Card Payment", url: [] },
                    { subtitle: "Insta Loan", url: [] },
                    { subtitle: "SmartEMI", url: [] },
                    { subtitle: "Insta Jumbo Loan", url: [] },
                    { subtitle: "Balance transfer on EMI", url: [] },
                    { subtitle: "Request Add on Credit Card", url: [] },
                    { subtitle: "Smart Pay", url: [] },
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    //{ subtitle: "", url: [] },
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "Credit Card Hotlisting", url: [] },
                    { subtitle: "Instant Pin Generation", url: [] },
                    { subtitle: "Set Card Controls/Usage", url: [] },
                    { subtitle: "Credit Limit Enhancement", url: [] },
                    { subtitle: "Credit Card Upgrade", url: [] },
                    { subtitle: "Register New Card", url: [] },
                    { subtitle: "Deregister Card", url: [] },
                    { subtitle: "Autopay Register", url: [] },
                    { subtitle: "Autopay De-register", url: [] },
                    { subtitle: "Credit Card ATM PIN", url: [] },
                    { subtitle: "Request Duplicate Statement", url: [] },
                    { subtitle: "Transferred Card Registration", url: [] },
                    { subtitle: "Physical Statement Suppression", url: [] },
                    { subtitle: "International Travel Details", url: [] },
                    { subtitle: "Generate PayZapp PIN", url: [] },
                ]
            },
        ],
        "Debit Cards": [
            {
                title: "Debit Card Status",
                url: ["dashboard", "cards", "debit-card-status"]
            },
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "Debit Card Upgrade", url: [] },
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    { subtitle: "Cash Back Enquiry and Redemption", url: [] },
                    { subtitle: "What's New", url: [] },
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "Debit Card Hotlisting", url: [] },
                    { subtitle: "Instant Pin Generation", url: [] },
                    { subtitle: "Set Card Controls/Usage Limits", url: [] },
                    { subtitle: "Re-issuance of Hotlisted Cards", url: [] },
                    { subtitle: "Account Linking to Debit Card", url: [] },
                    { subtitle: "Pin Regeneration", url: [] },
                ]
            },
        ],
        "Prepaid Cards": [
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "Reload Forex Card", url: [] },
                    { subtitle: "Purchase Gift Card", url: [] },
                    { subtitle: "Purchase Forex Card", url: [] },
                    { subtitle: "Reload General Purpose Card", url: [] },
                ]
            },
        ]
    },
    "demat": {
        "Demat": []
    },
    "mutual-fund": {
        "Mutual Fund": [
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "New Fund Offer", url: [] },
                    { subtitle: "Purchase", url: [] },
                    { subtitle: "Redeem", url: [] },
                    { subtitle: "Switch", url: [] },
                    { subtitle: "Systematic Investment", url: [] },
                    { subtitle: "Systematic Withdrawal", url: [] },
                    { subtitle: "Systematic Transfer", url: [] },
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    { subtitle: "Order Status", url: [] },
                    { subtitle: "Transaction History", url: [] },
                    { subtitle: "Unit Holding Statement", url: [] },
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "SI Order Status / Revocation", url: [] },
                    { subtitle: "Open Mutual Fund (ISA) Account", url: [] },
                ]
            },
        ]
    },
    "insurance": {
        "Insurance": [
            {
                title: "Life Insurance",
                url: ["dashboard", "insurance", "home"],
            },
            {
                title: "HDFC Life",
                url: null,
                items: [
                    { subtitle: "Buy Policis", url: [] },
                    { subtitle: "View Existing Policies", url: [] },
                    { subtitle: "Register Policy", url: [] },
                    { subtitle: "Renew Policies", url: [] },
                ]
            },
            {
                title: "Aditiya Birla Sun Life Insurance",
                url: ["dashboard", "insurance", "abs-life-insurance"],
            },
            {
                title: "TATA AIA Life",
                url: ["dashboard", "insurance", "tata-aia-life"],
            },
            {
                title: "General Insurance",
                url: null,
                items: [
                    { subtitle: "HDFC ERGO", url: [] },
                    { subtitle: "ICICI Lombard", url: [] },
                    { subtitle: "Bajaj Allianz", url: [] },
                ]
            },
            {
                title: "Health Insurance",
                url: null,
                items: [
                    { subtitle: "Aditya Birla Health", url: [] },
                    { subtitle: "Niva Bupa Health", url: [] },
                ]
            },
            {
                title: "Social Security Schemes",
                url: null,
                items: [
                    { subtitle: "Social Security Schemes - Jan Suraksha", url: [] },
                ]
            },
        ]
    },
    "loans": {
        "Loans": [
            {
                title: "Loan Summary",
                url: ["dashboard", "loans", "home"],
            },
            {
                title: "Transact",
                url: null,
                items: [
                    { subtitle: "Loan Insurance", url: [] },
                    { subtitle: "Renewal of Gold Loan", url: [] },
                    { subtitle: "Part Payment of Gold Loan", url: [] },
                    { subtitle: "Apply For Loan", url: [] },
                ]
            },
            {
                title: "Enquire",
                url: null,
                items: [
                    { subtitle: "Transaction History", url: [] },
                    { subtitle: "Personal Details", url: [] },
                    { subtitle: "Provisional Interest Statement", url: [] },
                    { subtitle: "Query/Feedback", url: [] },
                    { subtitle: "Loan Repayment Schedule", url: [] },
                    { subtitle: "Loan Account Details", url: [] },
                    { subtitle: "Online Transaction History", url: [] },
                    { subtitle: "Future Repayment Details", url: [] },
                ]
            },
            {
                title: "Request",
                url: null,
                items: [
                    { subtitle: "Interest Rate Changes", url: [] },
                    { subtitle: "Welcome Letter", url: [] },
                    { subtitle: "Register New Loan", url: [] },
                    { subtitle: "De-Register Loan", url: [] },
                    { subtitle: "Annual Loan Statement", url: [] },
                    { subtitle: "Finalv Interest Certificate", url: [] },
                    { subtitle: "Download Requested Letter", url: [] },
                    { subtitle: "Search Loan Account through Vehicle No.", url: [] },
                    { subtitle: "Dispatch Detail for Closure Documents", url: [] },
                    { subtitle: "Update Insurance Coverage for Vehicle", url: [] },
                    { subtitle: "Update Contact Details", url: [] },
                    { subtitle: "PAN Updation", url: [] },
                    { subtitle: "Update Vehicle Details", url: [] },
                    { subtitle: "NOC / Closure Letter", url: [] },
                    { subtitle: "Pay outstanding in Matured Loans", url: [] },
                    { subtitle: "Pay Overdue EMI and Charges", url: [] }, 
                ]
            },
        ]
    },
    "offers": {
        "Offers": []
    },
};