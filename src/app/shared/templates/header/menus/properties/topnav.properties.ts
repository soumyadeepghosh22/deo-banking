export const topnav = [
    { title: "Accounts", url: ["dashboard", "accounts"] },
    { title: "Funds Transfer", url: ["dashboard", "funds-transfer"] },
    { title: "BillPay & Recharge", url: ["dashboard", "billpay-recharge"] },
    { title: "Cards", url: ["dashboard", "cards"] },
    { title: "Demat", url: ["dashboard", "demat"] },
    { title: "Mutual Fund", url: ["dashboard", "mutual-fund"] },
    { title: "Insurance", url: ["dashboard", "insurance"] },
    { title: "Loans", url: ["dashboard", "loans"] },
    { title: "Offers", url: ["dashboard", "offers"] },
];