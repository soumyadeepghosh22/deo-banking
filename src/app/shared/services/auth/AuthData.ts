export interface AuthData {
    authCustomer: string;
    authToken: string;
}