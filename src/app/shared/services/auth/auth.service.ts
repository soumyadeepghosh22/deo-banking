import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment } from '@angular/router';
import { BehaviorSubject, map, take } from 'rxjs';
import { HttpService } from 'src/app/services/http/http.service';
import { AuthData } from './AuthData';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanLoad {

  constructor(
    private httpService: HttpService,
    private router: Router
  ) { }

  authData:BehaviorSubject<any> = new BehaviorSubject<any>('');

  canLoad(
    route: Route,
    segments: UrlSegment[]): boolean {
      if(!this.isAuthenticated()){
        const url = segments.reduce((path, currentSegment) => {
          return `${path}/${currentSegment.path}`;
        }, '');
        this.router.navigate(['login'], {queryParams: {url}});
      }
      return this.isAuthenticated();
  }

  isAuthenticated(){
    const authToken = this.getAuthInfo().authToken;
    this.authData.next(authToken);
    return authToken ? true : false;
  }

  getAuthInfo():AuthData{
    const authCustomer = sessionStorage.getItem('authCustomer') || "";
    const authToken = sessionStorage.getItem('authToken') || "";
    return {authCustomer, authToken};
  }

  setAuthInfo(data:AuthData){
    sessionStorage.setItem('authCustomer', data.authCustomer);
    sessionStorage.setItem('authToken', data.authToken);
  }

  removeAuthInfo(){
    sessionStorage.removeItem('authCustomer');
    sessionStorage.removeItem('authToken');
  }

  checkCustomerIdExist(customerId:any){
    return this.httpService.post('auth/check-username', { username: customerId });
  }

  login(req:any){
    return this.httpService.post('auth/login', {
      username: req.customerId,
      password: req.password
    });
  }

  getUserInfo(customerId:string){
    return this.httpService.post('dashboard/welcome', {
      username: customerId
    })
    .pipe(
      take(1),
      map((res:any) => {
        if(res && res.data && res.data.lastLoginTime){
          res.data.lastLoginTime = moment(res.data.lastLoginTime).format('lll');
        }
        return res;
      })
    );
  }

  logout(customerId:string){
    return this.httpService.post('auth/logout', {
      username: customerId
    })
  }
}
