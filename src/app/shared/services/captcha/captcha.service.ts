import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CaptchaService {

  constructor() { }

  private charArray:any[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
  private captcha:string = '';
  captchaData:BehaviorSubject<string> = new BehaviorSubject<string>('');

  get getCaptcha(){
    const data = this.generateCaptcha();
    return data;
  }

  generateCaptcha(){
    let charArray = [...this.charArray];
    for(let i = this.charArray.length-1; i > 0; i--){
      let j = Math.floor(Math.random()*(i + 1));
      let temp = charArray[i];
      charArray[i] = charArray[j];
      charArray[j] = temp;
    }
    this.captcha = charArray.join('').substring(0, 6);
    this.captchaData.next(this.captcha);
    return this.captcha;
  }

  validateCaptcha(data:string){
    return (this.captcha === data) ? true : false;
  }
}
