import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mask'
})
export class MaskPipe implements PipeTransform {

  transform(value:any, start:number, end:number): string {
    let valueArray = value.toString().split('');
    for(let i=start-1; i<end; i++){
      if(valueArray[i]){
        valueArray[i] = 'X';
      }
    }
    let maskedValue = valueArray.join('');
    return maskedValue;
  }

}
