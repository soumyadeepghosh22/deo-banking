import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedRoutingModule } from './shared-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MobileNumberDirective } from './directives/mobile-number/mobile-number.directive';
import { CustomerIdDirective } from './directives/customer-id/customer-id.directive';

import { MaskPipe } from './pipes/mask/mask.pipe';

import { HeaderComponent } from './templates/header/header.component';
import { FooterComponent } from './templates/footer/footer.component';
import { PrintPageComponent } from './print-page/print-page.component';
import { PageTitleComponent } from './page-title/page-title.component';
import { CardComponent } from './card/card.component';
import { TopnavComponent } from './templates/header/topnav/topnav.component';
import { SidenavComponent } from './templates/header/sidenav/sidenav.component';
import { PopupComponent } from './popup/popup.component';
import { CaptchaComponent } from './captcha/captcha.component';

@NgModule({
  declarations: [
    MobileNumberDirective,
    CustomerIdDirective,
    MaskPipe,
    HeaderComponent,
    FooterComponent,
    PrintPageComponent,
    PageTitleComponent,
    CardComponent,
    TopnavComponent,
    SidenavComponent,
    PopupComponent,
    CaptchaComponent,
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    PageTitleComponent,
    PrintPageComponent,
    CardComponent,
    PopupComponent,
    CaptchaComponent,
    MobileNumberDirective,
    CustomerIdDirective,
    MaskPipe,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule { }
