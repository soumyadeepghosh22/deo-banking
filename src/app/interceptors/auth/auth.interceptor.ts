import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, catchError, throwError } from 'rxjs';
import { AuthService } from '../../shared/services/auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const authToken = this.authService.getAuthInfo().authToken;
    if(authToken){
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${authToken}`
        }
      });
    }else{
      request = request.clone({
        setHeaders: {
          'Content-Type': 'application/json',
        }
      });
    }
    return next.handle(request)
    .pipe(
      catchError((err:any) => {
        if(err instanceof HttpErrorResponse){
          let url = window.location.pathname;
          if(err.status === 401 && url.includes('dashboard')){
            this.authService.removeAuthInfo();
            window.location.reload();
          }
        }
        return throwError(err);
      })
    )
  }
}
